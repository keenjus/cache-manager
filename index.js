var CacheEntry = /** @class */ (function () {
    function CacheEntry(data, time) {
        this.data = data;
        this.time = time;
    }
    return CacheEntry;
}());
var CacheManager = /** @class */ (function () {
    function CacheManager() {
    }
    CacheManager.prototype.getOrCreate = function (key, expireIn, creator) {
        var _this = this;
        return new Promise(function (resolve) {
            var existing = JSON.parse(localStorage.getItem(key));
            if (existing && !_this.isExpired(existing.time, expireIn)) {
                console.log("Loaded " + key + " data from cache");
                resolve(existing.data);
            }
            else {
                creator().then(function (data) {
                    localStorage.setItem(key, JSON.stringify(new CacheEntry(data, Date.now())));
                    console.log("Cached " + key + " data");
                    resolve(data);
                });
            }
        });
    };
    CacheManager.prototype.isExpired = function (time, expireIn) {
        return Date.now() - time > expireIn;
    };
    return CacheManager;
}());
